## How many % of adverts and trackers are using Cloudflare?


- [Easylist](https://web.archive.org/web/20210516110248/https://easylist.to/)
```
EasyList is the primary filter list that removes most adverts from international webpages, including unwanted frames, images and objects.

EasyPrivacy is an optional supplementary filter list that completely removes all forms of tracking from the internet, including web bugs, tracking scripts and information collectors, thereby protecting your personal data.
```


We picked domain-blocking lines from the list and filtered out domains which has exception rules.
Here's the result.


| Adblock list | Domains Count | Cloudflare | % |
| --- | --- | --- | --- |
| [EasyList](https://easylist.to/easylist/easylist.txt) | 51,331 | 12,729 | 24.8% |
| [EasyPrivacy](https://easylist.to/easylist/easyprivacy.txt) | 33,565 | 13,628 | 40.6% |
| [Annoyance](https://secure.fanboy.co.nz/fanboy-annoyance.txt) | 22,774 | 9,988 | 43.9% |
| [AdGuard](https://adguardteam.github.io/AdGuardSDNSFilter/Filters/filter.txt) | 62,578 | 12,178 | 19.5% |
| [AdAway](https://raw.githubusercontent.com/AdAway/adaway.github.io/master/hosts.txt) | 1,957 | 842 | 43% |
| Total | 119,265 | 36,711 | 30.8% |


### 30.8% of adverts and trackers are using Cloudflare.