## How many % of domains listed in DNS filter/blocklists are using Cloudflare?


Below are some of public blocklists to block ads, affiliate, tracking, metrics, telemetry etc.
We picked domain lines from the list and filtered out duplicates.
Here's the result.


| Blocklist | Domains Count | Cloudflare | % |
| --- | --- | --- | --- |
| [1Hosts.Lite](https://raw.githubusercontent.com/badmojr/1Hosts/master/Lite/hosts.win) | 68,819 | 21,884 | 31.8% |
| [1Hosts.Mini](https://raw.githubusercontent.com/badmojr/1Hosts/master/mini/hosts.win) | 67,377 | 21,345 | 31.68% |
| [1Hosts.Pro](https://raw.githubusercontent.com/badmojr/1Hosts/master/Pro/hosts.win) | 122,545 | 41,271 | 33.68% |
| [AdAway](https://raw.githubusercontent.com/AdAway/adaway.github.io/master/hosts.txt) | 1,953 | 841 | 43.06% |
| [AdGuard](https://adguardteam.github.io/AdGuardSDNSFilter/Filters/filter.txt) | 62,578 | 12,155 | 19.42% |
| [HaGeZi.LIGHT](https://raw.githubusercontent.com/hagezi/dns-blocklists/main/hosts/light.txt) | 53,810 | 15,968 | 29.67% |
| [HaGeZi.NORMAL](https://raw.githubusercontent.com/hagezi/dns-blocklists/main/hosts/multi.txt) | 117,140 | 33,051 | 28.21% |
| [HaGeZi.PROPLUS](https://raw.githubusercontent.com/hagezi/dns-blocklists/main/hosts/pro.plus.txt) | 152,636 | 44,079 | 28.88% |
| [HaGeZi.PRO](https://raw.githubusercontent.com/hagezi/dns-blocklists/main/hosts/pro.txt) | 130,330 | 36,393 | 27.92% |
| [HaGeZi.ULTIMATE](https://raw.githubusercontent.com/hagezi/dns-blocklists/main/hosts/ultimate.txt) | 159,558 | 46,668 | 29.25% |
| [OISD.B](https://big.oisd.nl/dnsmasq) | 207,201 | 60,641 | 29.27% |
| [OISD.N](https://nsfw.oisd.nl/dnsmasq) | 344,740 | 144,206 | 41.83% |
| [OISD.S](https://small.oisd.nl/dnsmasq) | 43,737 | 9,136 | 20.89% |
| [StevenBlack.UF](https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/fakenews/hosts) | 57,044 | 19,826 | 34.76% |
| [StevenBlack.UG](https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/gambling/hosts) | 60,347 | 21,870 | 36.24% |
| [StevenBlack.UP](https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/porn/hosts) | 100,774 | 42,415 | 42.09% |
| [StevenBlack.US](https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/social/hosts) | 55,015 | 19,281 | 35.05% |
| Total | 708,694 | 251,368 | 35.47% |


### 35.47% of domains listed in DNS filter/blocklists are using Cloudflare.